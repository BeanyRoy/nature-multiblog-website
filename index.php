<?php

    session_start();

    //Nur zur Kontrolle der Session
    echo '<b>Session ID:</b> '.session_id();

    require_once '../fairvressen_api/api/objects/entry.php';
    require_once '../fairvressen_api/api/objects/user.php';

    $output = file_get_contents('html/index.html');
    
    // $response = file_get_contents('http://localhost:81/priv_projects/fairvressen_api/api/v1/entries/')
    $response = file_get_contents('http://localhost:81/priv_projects/fairvressen_api/sample_responses/v1/GET_entries.json');

    // if (isset($response))
    // {
    //     $entryArray = json_decode($response, true);
    //     if (sizeof($entryArray) > 0)
    //     {
    //         $topEntriesOutput = '';
    //         foreach ($entryArray as $e)
    //         {
    //             $topEntryTpl = file_get_contents('html/tpl/top_entry_tpl.html');
    //             $topEntryTpl = str_replace('{{id}}', $e['id'], $topEntryTpl);
                
    //             //Vielleicht das Datum eleganter lösen
    //             $topEntryCreationDateArray = getdate($e['date']);
    //             $topEntryCreationDate = $topEntryCreationDateArray['mday'].'.'.$topEntryCreationDateArray['mon'].'.'.$topEntryCreationDateArray['year'];
    //             $topEntryTpl = str_replace('{{date}}', $topEntryCreationDate, $topEntryTpl);
                
    //             $topEntryTpl = str_replace('{{name}}', $e['name'], $topEntryTpl);
    //             $topEntryTpl = str_replace('{{creator_name}}', $e['creator']['name'], $topEntryTpl);
    //             $topEntryTpl = str_replace('{{likes}}', $e['likes'], $topEntryTpl);
    //             $topEntryTpl = str_replace('{{comments_count}}', sizeof($e['comments']), $topEntryTpl);

    //             $topEntriesOutput .= $topEntryTpl;

    //         }
    //         $output = str_replace('{{top_entries}}', $topEntriesOutput, $output);
    //     }
    // }

    echo $output;
    
?>